const CACHE_NAME = "aniplace_v6"
const urlToCache = [
    "/",
    "/manifest.json",
    "/nav.html",
    "/index.html",
    "/pages/galery.html",
    "/pages/seiyuu.html",
    "/pages/home.html",
    "/pages/skill.html",
    "/css/materialize.min.css",
    "/css/style.css",
    "/js/materialize.min.js",
    "/js/nav.js",
    "/public/img/megumin.jpg",
    "/public/img/1.jpg",
    "/public/img/2.jpg",
    "/public/img/3.jpg",
    "/public/img/4.jpg",
    "/public/img/5.jpg",
    "/public/img/6.jpg",
    "/public/img/7.jpg",
    "/public/img/gif.gif",
    "/public/img/rie.jpg",
    "/public/icons/logo.png",
    "/public/icons/logo-192.png",
    "/public/icons/logo-96.png",
    "/public/icons/logo-72.png",
    "/public/icons/logo-48.png"
]

self.addEventListener('install', (e) => {
    e.waitUntil(
        caches.open(CACHE_NAME)
         .then((cache) => {
             return cache.addAll(urlToCache)
         })
    )
})

self.addEventListener('fetch', (e) => {
    e.respondWith(
        caches
         .match(e.request, { cacheName: CACHE_NAME })
         .then((response) => {
            if(response){
                console.log(`Service worker: using asset from cache: ${response.url}`)
                return response
            }
            console.log(`Service worker loaded asset from server: ${e.request.url}`)
            return fetch(e.request)
        })
    )
})

self.addEventListener('activate', (e) => {
    e.waitUntil(
        caches.keys()
         .then((cacheNames) => {
             return Promise.all(
                 cacheNames.map((cacheName) => {
                    if(cacheName != CACHE_NAME){
                        console.log(`Serviceworker: cache ${cacheName} has been deleted`)
                        return caches.delete(cacheName)
                    }
                 })
             )
         })
    )
})