document.addEventListener('DOMContentLoaded', function(){
    const sidenav = document.querySelector('.sidenav')
    M.Sidenav.init(sidenav)
    let page = window.location.hash.substr(1)
    if(page == "") page = "home"
    loadNav()
    loadPage(page)

    function loadNav(){
        const xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            if(this.readyState == 4){
                if(this.status != 200) return

                document.querySelectorAll('.sidenav, .topnav').forEach((elm) => {
                    elm.innerHTML = xhttp.responseText
                })

                document.querySelectorAll('.sidenav a, .topnav a').forEach((elm) => {
                    elm.addEventListener('click', (e) => {
                        const sidenav = document.querySelector('.sidenav')
                        M.Sidenav.getInstance(sidenav).close()

                        page = e.target.getAttribute('href').substr(1)
                        loadPage(page)
                    })
                })
            }
        }
        xhttp.open("GET", "nav.html", true)
        xhttp.send()
    }

    function loadPage(page){
        const xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4){
                if(this.readyState == 4){
                    let content = document.querySelector('#body-content')
                    if(this.status == 200){
                        content.innerHTML = xhttp.responseText
                    }else if(this.status == 404){
                        content.innerHTML = `<h3 class='center'> Page not found</h3>`
                    }else{
                        content.innerHTML = `<h3 class='center'>Something went wrong</h3>`
                    }
                }
            }
        }
        xhttp.open("GET", `pages/${page}.html`, true)
        xhttp.send()
    }
})
